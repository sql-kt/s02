--To run the MySQL/Maria DB
mysql -u root
-- default username si root
-- -p is password mysql -u root -p
-- semi colon end ng syntax
--not case sensitive
-- constraint start ng fk or foreign key
-- CASCADE SET NULL DEFAULT
-- CASCADE, if may naupdate sa table, magupdate rin sya
--ON DELETE NULL, RESTRICT
-- ma-avoid ang biglaang deletion ng table
-- di basta basta madrop ang table

mysql -u root

-- SHOW DATABASES
SHOW DATABASES;
show databases;

-- CREATE DATABASE

SELECT -- extracts data from a database
UPDATE -- updates data in a database
DELETE -- deletes data from a database
INSERT INTO -- inserts new data into a database
CREATE DATABASE -- creates a new database
ALTER DATABASE -- modifies a database
CREATE TABLE -- creates a new table
ALTER TABLE -- modifies a table
DROP TABLE -- deletes a table
USE -- use db
SHOW -- show tables